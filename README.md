# Flipstarter: frontend

Website to display flipstarter assurance contract campaigns and interact with the flipstarter backend.

## Requirements

To run the flipstarter frontend, you will need [**Git**](https://git-scm.com/), [**NodeJS**](https://nodejs.org/) and the [**NPM**](https://www.npmjs.com/) applications.

## Installation

Start by cloning the repository or downloading a released zipfile:

```bash
git clone https://gitlab.com/flipstarter/frontend/
```

Then change your working directory to match the downloaded files:

```bash
cd frontend/
```

Proceed to installing all library dependencies:

```bash
npm install
```

When all dependencies are installed, you need to generate the javascript:

```bash
./webpack.sh
```

Configure your preferred webserver and set it to use `frontend/www` as the root folder.

## Usage

After you have installed the backend and frontend, you can open `frontend/www/index.html` in a browser to see the website.
