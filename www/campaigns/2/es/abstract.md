## Help us deliver food for one month

Over the last two years we have served cooked meals and delivered food bags to people in need and we now ask for your help to secure one more month of operation.

- In Venezuela, you support **25** volunteers providing food in **9 locations**.
- In South Sudan, you support **13** volunteers providing food in **11 locations**.

We are a group of friends doing what we can to help our neighbors in this time of uncertainty. What better way to bond with your neighbors than over a hot meal prepared with love?
